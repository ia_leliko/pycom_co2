const admin = require('firebase-admin');
admin.initializeApp();
var db = admin.firestore();

/**
 * Triggered from a message on a Cloud Pub/Sub topic.
 *
 * @param {!Object} event Event payload.
 * @param {!Object} context Metadata for the event.
 */
exports.pycomCO2Telemetry = (event, context) => {
  const pubsubMessage = event.data;

  if (!pubsubMessage) {
    throw new Error('No telemetry data was provided!');
  }

  const payload = Buffer.from(pubsubMessage, 'base64').toString();

  console.log(payload);

  const telemetry = JSON.parse(payload);
  const attributes = event.attributes;
  const deviceId = attributes.deviceId;

  db.collection(`devices/${deviceId}/measurements`).add({
    'timestamp': 1000*telemetry.timestamp,
    'date': new Date(1000*telemetry.timestamp),
    'uid': telemetry.uid,
    'co2': telemetry.co2,
    'rh':telemetry.rh,
    'temp':telemetry.temp,
    'voc':telemetry.voc
  }).then((writeResult) => {
    console.log({'result': 'Message with ID: ' + writeResult.id + ' added.'});
    return;
  }).catch((err) => {
    console.log(err);
    return;
  });
};
