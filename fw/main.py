import json
import _thread
import time
import pycom
import config
import network
import influxdb
import gc

from machine import RTC
from connection import connectionWiFi
from web_server import http_server_run
from sensors import Sensors

INITIAL_COLOR = 0x050000
CONNECTED_TO_NETWORK_COLOR = 0x000005
CONNECTED_TO_CLOUD_COLOR = 0x000500

pycom.heartbeat(False)

pycom.rgbled(INITIAL_COLOR)

conn = connectionWiFi()
network.Server()


def WiFi_thread():
    print("WiFi Thread")
    while True:
        try:
            if not conn.isConnected():
                conn.connect()
            time.sleep(5)
        except Exception as err:
            print("Connection err: {}".format(err))


_thread.start_new_thread(WiFi_thread, ())

while not conn.isConnected():
    time.sleep(1)

pycom.rgbled(CONNECTED_TO_NETWORK_COLOR)

rtc = RTC()
rtc.ntp_sync("pool.ntp.org")
while not rtc.synced():
    time.sleep(0.25)
print('System time: {}'.format(rtc.now()))

# Run sensors
sensors = Sensors()

# Run web server
http_server_run()


def send_env_data():
    while True:
        try:
            if not conn.isConnected():
                pycom.rgbled(INITIAL_COLOR)
                while not conn.isConnected():
                    time.sleep(1)
                pycom.rgbled(CONNECTED_TO_NETWORK_COLOR)
            sensors.measure()
            r = influxdb.send_data(sensors.get_data())
            if r.status_code != 204:
                print("Response status: {}, reason{}".format(
                    r.status_code,
                    r.reason
                ))
            gc.collect()
            pycom.rgbled(CONNECTED_TO_CLOUD_COLOR)
            time.sleep(60)
        except Exception as e:
            print(e)


_thread.start_new_thread(send_env_data, ())
