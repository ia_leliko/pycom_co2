var app = new Vue({
    data: {
        co2: null,
        voc: null
    },
    mounted() {
        axios
            .get('/sensors')
            .then(response => {
                this.co2 = "co2: " + response.data.sensors.co2 + " ppm";
                this.voc = "voc: " + response.data.sensors.voc + " ppm";
            });
    }
}).$mount('#app')
