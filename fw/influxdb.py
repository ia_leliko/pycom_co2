import config
from third_party import urequests


def send_data(data: dict):
    headers = {
        "Authorization": "Token {}".format(config.influxdb["token"]),
    }
    url = url = "{}/api/v2/write?org={}&bucket={}&precision={}".format(
        config.influxdb["url"],
        config.influxdb["org"],
        config.influxdb["bucket"],
        "s")
    r = urequests.post(url, data=influx_format(data), headers=headers)
    return r


def influx_format(data: dict):
    output_str = 'pycom-co2 {}="{}",'.format("uid", data["uid"])
    output_str = "{}{}={},".format(output_str, "timestamp", data["timestamp"])
    output_str = "{}{}={},".format(output_str, "co2", data["co2"])
    output_str = "{}{}={},".format(output_str, "voc", data["voc"])
    output_str = "{}{}={},".format(output_str, "temp", data["temp"])
    output_str = "{}{}={},".format(output_str, "rh", data["rh"])
    output_str = "{}{}={},".format(output_str, "base_co2", data["base"][0])
    output_str = "{}{}={}".format(output_str, "base_voc", data["base"][1])
    output_str = "{} {}".format(output_str, data["timestamp"])

    return output_str
