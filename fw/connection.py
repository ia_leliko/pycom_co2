import time
import config
from machine import Pin
from network import LTE, WLAN


class connection:
    def connect(self) -> bool:
        pass

    def disconnect(self):
        pass

    def isConnected(self) -> bool:
        pass


class connectionWiFi(connection):
    def __init__(self):
        Pin('P12', mode=Pin.OUT)(True)
        # To enable ext. antenna add "antenna=WLAN.EXT_ANT"
        self._wlan = WLAN(mode=WLAN.STA)
        self._wlan.ifconfig(config=(
            config.wifi_config['ip'],
            config.wifi_config['nm'],
            config.wifi_config['gw'],
            config.wifi_config['dns']
        ))
        self._ssid = config.wifi_config['ssid']
        self._password = config.wifi_config['password']
        self.nets = None

    def connect(self) -> bool:
        self.nets = self._wlan.scan()
        self._wlan.connect(ssid=self._ssid, auth=(WLAN.WPA2, self._password))
        while not self._wlan.isconnected():
            time.sleep(1)
        print(self._wlan.ifconfig())
        return True

    def disconnect(self):
        pass

    def isConnected(self) -> bool:
        return self._wlan.isconnected()


class connectionLTE(connection):
    def __init__(self):
        self._lte = lte = LTE()         # instantiate the LTE object

    def send_at_cmd_pretty(self, cmd):
        response = self._lte.send_at_cmd(cmd).split('\r\n')
        for line in response:
            print(line)

    def connect(self) -> bool:
        if self._lte.isconnected():
            print('Already connected to LTE network')
            return
        self.send_at_cmd_pretty('AT+CMEE=2')
        self._lte.attach(band=20, apn="hologram")
        while not self._lte.isattached():
            time.sleep(1)
        print('Attached to LTE network')

        self._lte.connect()       # start a data session and obtain an IP address
        while not self._lte.isconnected():
            time.sleep(0.25)
        print('Connected to LTE network')
        return True

    def disconnect(self):
        pass

    def isConnected(self) -> bool:
        rez = self._lte.isconnected()
        # Workaround
        self._lte.pppsuspend()
        self._lte.pppresume()
        return rez
