import ubinascii
import machine
import time
import pycom
import adafruit_sgp30

from machine import I2C, Timer


def singleton(cls):
    instances = {}

    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance


@singleton
class Sensors:
    def __init__(self):
        self.i2c = I2C(0, I2C.MASTER)
        self.i2c.init(I2C.MASTER, baudrate=100000)

        # Create library object on our I2C port
        self.sgp30 = adafruit_sgp30.Adafruit_SGP30(self.i2c)
        self.sgp30.iaq_init()
        co2 = self.get_baseline_co2()
        voc = self.get_baseline_voc()
        if co2 != 0 and voc != 0:
            self.sgp30.set_iaq_baseline(co2, voc)
        print("SGP30 serial #", [hex(i) for i in self.sgp30.serial])
        self.alarm = Timer.Alarm(self.baseline_handler, 43200, periodic=True)

        self.data = dict()

    def get_baseline_co2(self):
        try:
            return pycom.nvs_get("co2")
        except ValueError:
            return 0

    def get_baseline_voc(self):
        try:
            return pycom.nvs_get("voc")
        except ValueError:
            return 0

    def measure(self):
        self.data["uid"] = ubinascii.hexlify(machine.unique_id())
        self.data["timestamp"] = time.time()
        self.data["co2"] = self.sgp30.co2eq
        self.data["voc"] = self.sgp30.tvoc
        self.data["temp"] = 18
        self.data["rh"] = 65
        self.data["base"] = self.sgp30.get_iaq_baseline()
        self.data["stored_base"] = [
            self.get_baseline_co2(),
            self.get_baseline_voc()
        ]

    def get_data(self):
        return self.data

    def baseline_handler(self, alarm):
        pycom.nvs_set("co2", self.data["base"][0])
        pycom.nvs_set("voc", self.data["base"][1])

    def reset_baseline(self):
        pycom.nvs_set("co2", 0)
        pycom.nvs_set("voc", 0)
