import pycom
import uos

from MicroWebSrv2 import *
from time import sleep, gmtime
from sensors import Sensors


def http_server_run():
    mws2 = MicroWebSrv2()
    mws2.StartManaged()


@WebRoute(GET, '/sensors')
def RequestData(microWebSrv2, request):
    request.Response.ReturnOkJSON({
        "sensors": Sensors().get_data(),
        "time": gmtime(),
        "memory": pycom.get_free_heap(),
        "flash": uos.getfree("/flash")
    })


@WebRoute(GET, '/reset')
def RequestData(microWebSrv2, request):
    Sensors().reset_baseline()
    request.Response.ReturnOkJSON({
        "co2": Sensors().get_baseline_co2(),
        "voc": Sensors().get_baseline_voc()
    })
