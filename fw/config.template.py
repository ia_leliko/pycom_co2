# Configuration File

wifi_config = {
    'ssid': '',
    'password': '',
    'ip': '192.168.0.220',
    'nm': '255.255.255.0',
    'gw': '192.168.0.1',
    'dns': '192.168.0.1'
}

google_cloud_config = {
    'project_id': 'pycom-co2',
    'cloud_region': 'europe-west1',
    'registry_id': 'pycom-co2',
    'device_id': 'pycom-co2',
    'mqtt_bridge_hostname': 'mqtt.googleapis.com',
    'mqtt_bridge_port': 8883
}

influxdb = {
    "token": "",
    "org": "a.papkovskiy@gmail.com",
    "bucket": "pycom-co2-bucket",
    "url": "https://eu-central-1-1.aws.cloud2.influxdata.com"
}

jwt_config = {
    'algorithm': 'RS256',
    'token_ttl': 43200,  # 12 hours
    # Use utiles/decode_rsa.py to decode private pem to pkcs1.
    'private_key': ()
}
