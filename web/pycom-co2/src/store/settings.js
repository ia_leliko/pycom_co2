export default {
    state: {
        locale: "en"
    },
    mutations: {
        setLocale(state, locale) {
            state.locale = locale
        }
    },
    actions: {

    },
    getters: {
        locale: (state) => {
            return state.locale
        }
    },
}
