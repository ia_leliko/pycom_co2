import Vue from 'vue'
import Vuex from 'vuex'
import userModule from './user'
import settingsModule from './settings'
import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
  key: 'app-settings',
  storage: window.localStorage,
  modules: ['settings'],
})

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user: userModule,
    settings: settingsModule,
  },
  plugins: [vuexPersist.plugin]
})
