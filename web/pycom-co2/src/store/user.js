export default {
    state: {
        user: {
            isAuthenticated: false,
            data: null,
        }
    },
    mutations: {
        setUser(state, isAuthenticated, data) {
            state.user.isAuthenticated = isAuthenticated
            state.user.data = data
        }
    },
    actions: {

    },
    getters: {
        isAuthenticated: (state) => {
            return state.user.isAuthenticated
        }
    }
}
