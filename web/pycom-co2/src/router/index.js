import Vue from 'vue'
import VueRouter from 'vue-router'
import SignIn from '../views/SignIn.vue'
import SignOut from '../views/SignOut.vue'
import AppHome from '../views/AppHome.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: AppHome
  },
  {
    path: '/signin',
    name: 'signin',
    component: SignIn
  },
  {
    path: '/signout',
    name: 'signout',
    component: SignOut
  }
]

const router = new VueRouter({
  routes
})

export default router
